from Parser.ExperimentParser import JsonDictonaryToExperimentParser
from Parser.JsonParser import FromJsonFileToDictonaryParser
from Output.Ploting import CartesianSystemPlotting


parser = JsonDictonaryToExperimentParser()
JsonParser = FromJsonFileToDictonaryParser()
jsonDic = JsonParser.parse("jsonSchema/example_delta=0.1epsilon=0.1.json")
plotter = CartesianSystemPlotting()

plotter.xLabel = "entropia"
plotter.yLabel = "amostras minimas"
plotter.title = "experimento para delta=0.1 e epsilon=0.1"


experiment = parser.parse(jsonDic)
result = experiment.run()

for r in result:
	plotter.addXYPair(r[0], r[1])

plotter.plotOnImageFile("result_1_0.png")
print(result)