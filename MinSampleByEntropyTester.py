from Parser.ExperimentParser import JsonDictonaryToExperimentParser
from Output.HtmlOutput import ExperimentOutput
from Parser.JsonParser import FromJsonFileToDictonaryParser

import sys, getopt




def main(argv):
	configFile = ""
	outputFile = "result" 

	try:
		opts, args = getopt.getopt(argv, "hc:o:",["configFile=", "outputFile="])
	except getopt.GetoptError:
		print("Syntax: ")
		print("MinSampleByEntropyTester.py -c <configFile> -o <outputFile>")
		sys.exit(2)

	for opt, arg in opts:
		if opt == "-h":
			print("Syntax: ")
			print("MinSampleByEntropyTester.py -c <configFile> -o <outputFile>")
			sys.exit(0)
		elif opt in ("-c", "--configFile"):
			configFile = arg
		elif opt in ("-o", "--outputFile"):
			outputFile = arg


	jsonParser = FromJsonFileToDictonaryParser()
	experimentParser = JsonDictonaryToExperimentParser()
	experimentOutput = ExperimentOutput()
	jsonDic = jsonParser.parse(configFile)

	experiment = experimentParser.parse(jsonDic)
	result = experiment.run()	
	minSampleCalculated = experiment.minSampleCalculated()

	experimentOutput.createHtmlOutput(result, minSampleCalculated, experiment.delta(), experiment.epsilon(), configFile, outputFile)

	print("------------------------------ Experiment has been Finished ------------------------------------ ")

if __name__ == '__main__':
	main(sys.argv[1:])