from ProbabilityDistribution.DistributionModel import NormalDistributionModel, UniformDistributionModel
from ProbabilityDistribution.ProbabilityClass import ProbabilityObject
from Classifier.Classifier import Classifier, ClassifierGenerator, ClassifierVariable
from Classifier.Accumulator import AccumulatorLabel, AccumulatorProbabilityCalculator
from RestrictionControl.RestrictionControl import ConditionalProbabilityErrorMeasurer, RestrictionControl


mean = 3.0
variation = 4.01501501502
probabilitiesClasses = [ProbabilityObject("1", 1.0), ProbabilityObject("2", 2.0), ProbabilityObject("3", 3.0),
						ProbabilityObject("4", 4.0),ProbabilityObject("5", 5.0)]

dist = NormalDistributionModel(probabilitiesClasses, mean, variation)

print("pdf(" + str(mean) + ") = " + str(dist.pdf(mean)))
print("p(x = " + str(mean) + ") = " + str(dist.probability("3")))
print(dist.samples(50))

print()
print("UNIFORM MODEL")

xs = [ProbabilityObject("0000", 0), ProbabilityObject("0001", 1), ProbabilityObject("0010", 2), ProbabilityObject("0011", 3),
	  ProbabilityObject("0100", 4), ProbabilityObject("0101", 5), ProbabilityObject("0110", 6), ProbabilityObject("0111", 7), 
	  ProbabilityObject("1000", 8), ProbabilityObject("1001", 9), ProbabilityObject("1010", 10), ProbabilityObject("1011", 11),
	  ProbabilityObject("1100", 12), ProbabilityObject("1101", 13), ProbabilityObject("1110", 14), ProbabilityObject("1111", 15)]

uni = UniformDistributionModel(xs)
print(uni.samples(50))

cs = [ClassifierVariable(xs[0].label, dist), ClassifierVariable(xs[1].label, dist), ClassifierVariable(xs[2].label, dist),
	  ClassifierVariable(xs[3].label, dist), ClassifierVariable(xs[4].label, dist), ClassifierVariable(xs[5].label, dist),
	  ClassifierVariable(xs[6].label, dist), ClassifierVariable(xs[7].label, dist), ClassifierVariable(xs[8].label, dist),
	  ClassifierVariable(xs[9].label, dist), ClassifierVariable(xs[10].label, dist), ClassifierVariable(xs[11].label, dist),
	  ClassifierVariable(xs[12].label, dist), ClassifierVariable(xs[13].label, dist), ClassifierVariable(xs[14].label, dist),
	  ClassifierVariable(xs[15].label, dist)]

classifierGenerator = ClassifierGenerator(uni, cs, probabilitiesClasses)
optClassifier = classifierGenerator.generateOptimumClassifier()

print()
print("CLASSIFIER OPTIMUM")
optClassifier.debug()
print()
print("classifier mean entropy: " + str(optClassifier.meanEntropy()))


# print()
# print("ACCUMULATOR TEST")

# accLabels = []

# for x in xs:
# 	yLabels = []
# 	for y in probabilitiesClasses:
# 		yLabels.append(y.label)
	
# 	accLabels.append(AccumulatorLabel(x.label, yLabels))

# accumulator = AccumulatorProbabilityCalculator(accLabels)


# samplesNumber = 100

# for i in range(1, samplesNumber):
# 	x = uni.sample()
# 	xIndex = next(index for index, value in enumerate(xs) if value.label == x)
# 	y = cs[xIndex].sampleAClass()

# 	accumulator.accumulate(x, y)

# for x in xs:
# 	print("p(" + x.label + ") = " + str(accumulator.probability(x.label)))




delta = 0.1
epsilon = 0.1
spaceCardinality = 5**(2**4)
print()
print("MIN NUMBER OF SAMPLES FOR EPSILON = 0.1 AND DELTA = 0.1")

minNumberOfSamplesCalculated = classifierGenerator.minNumberOfSamples(epsilon, delta, spaceCardinality)
print("m(e, d) = " + str(minNumberOfSamplesCalculated))


print()
print("ESTIMATED CLASSIFIER FOR 1000 SAMPLES")
estimatedClassifier = classifierGenerator.generateClassifier(minNumberOfSamplesCalculated)
estimatedClassifier.debug()

errorMeasurer = ConditionalProbabilityErrorMeasurer()
restrictionControl = RestrictionControl(errorMeasurer, classifierGenerator, delta, epsilon)
print(restrictionControl.hasPassed(minNumberOfSamplesCalculated))