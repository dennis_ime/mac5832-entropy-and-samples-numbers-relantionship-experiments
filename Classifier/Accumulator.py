class AccumulatorLabel:
	def __init__(self, xLabel, yLabels):
		self.xLabel = xLabel
		self.yLabels = yLabels		

class AccumulatorProbabilityCalculator:
	def __init__(self, labels):
		self.labels = labels
		self._totalElements = 0
		self._accumulator = {}

		for label in self.labels:
			self._accumulator[label.xLabel] = {}

			for yLabel in label.yLabels:
				self._accumulator[label.xLabel][yLabel] = 0

	def accumulate(self, xLabel, yLabel):
		self._totalElements += 1		
		self._accumulator[xLabel][yLabel] += 1

	def _countXLabel(self, xLabel):
		count = 0
		for yLabel, yFreq in self._accumulator[xLabel].items():
			count += yFreq

		return count

	def probability(self, xLabel):
		xLabelCount = self._countXLabel(xLabel)
		return self.__safeDivision(xLabelCount, self._totalElements)

	def jointProbability(self, xLabel, yLabel):
		xLabelCount = self._countXLabel(xLabel)
		yLabelCount = self._accumulator[xLabel][yLabel]

		return self.__safeDivision(yLabelCount, xLabelCount)

	def __safeDivision(self, nomerator, denominator):
		if float(denominator) == 0.0:
			return 0.0

		return float(nomerator) / float(denominator)