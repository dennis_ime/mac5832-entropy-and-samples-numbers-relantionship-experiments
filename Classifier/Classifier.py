from Classifier.Accumulator import AccumulatorLabel, AccumulatorProbabilityCalculator
import math

class ProbabilityClass:
	def __init__(self, label, probability):
		self.label = label
		self.probability = probability

class RandomVariable:
	def __init__(self, variableLabel, variableProbability, probabilityClasses):
		self.variableLabel = variableLabel
		self.probabilityClasses = probabilityClasses
		self.variableProbability = variableProbability
		variableClass = self.pickUpMaxProbabilityVariableClass()
		self.variableClassLabel = variableClass.label
	
	def pickUpMaxProbabilityVariableClass(self):
		probabilityClass = self.probabilityClasses[0]
		for i in range(1, len(self.probabilityClasses) - 1):			
			if self.probabilityClasses[i].probability > probabilityClass.probability:
				probabilityClass = self.probabilityClasses[i]

		return probabilityClass

class ClassifierVariable:
	def __init__(self, label, distributionModelClass):
		self.label = label
		self.distributionModelClass = distributionModelClass

	def probabilityForClass(self, classLabel):
		return self.distributionModelClass.probability(classLabel)

	def sampleAClass(self):
		return self.distributionModelClass.sample()

	def getAllClasses(self):
		return self.distributionModelClass.probabilitiesClasses

	def isClassesModelDeterministic(self):
		return self.distributionModelClass.isDeterministic()

class Classifier:
	def __init__(self, randomVariables):
		self.randomVariables = randomVariables

	def toHtmlTable(self):
		classes = self.randomVariables[0].probabilityClasses
		header = "<tr><th>p(x)</th><th>x</th>"

		for c in classes:
			header += "<th>" + c.label + "</th>"

		header += "<th>classe</th></tr>"


		row = ""
		for x in self.randomVariables:
			row += "<tr><td>" + str(x.variableProbability) + "</td>"
			row += "<td>" + x.variableLabel + "</td>"

			for c in x.probabilityClasses:
				row += "<td>" + str(c.probability) + "</td>"

			row += "<td>" + x.variableClassLabel + "</td></tr>"

		return header + row


	def debug(self):
		classes = self.randomVariables[0].probabilityClasses
		header = "p(x) x "

		for c in classes:
			header += c.label + "   "		

		header += "  class"
		print(header)

		for x in self.randomVariables:
			row = str(x.variableProbability)
			row += " " + str(x.variableLabel)

			for c in x.probabilityClasses:
				row += " " + str(c.probability)

			row += " " + x.variableClassLabel
			print(row)

	def entropy(self, randomVariable):
		entropy = 0.0
		for c in randomVariable.probabilityClasses:
			entropy -= c.probability * math.log(c.probability, 2)

		return entropy

	def meanEntropy(self):
		meanEntropy = 0.0
		for rv in self.randomVariables:
			meanEntropy += rv.variableProbability * self.entropy(rv)

		return meanEntropy		


	def classify(self, drawnVariable):
		pass


class ClassifierGenerator:
	def __init__(self, randomVariableDistributionModel, classifierVariables, probabilityObjectForEachClass):
		self.randomVariableDistributionModel = randomVariableDistributionModel
		self.classifierVariables = classifierVariables
		self.probabilityObjectForEachClass = probabilityObjectForEachClass

	def getVariation(self):
		return self.classifierVariables[0].distributionModelClass.variation

	def generateOptimumClassifier(self):
		randomVariables = []
		for x in self.classifierVariables:
			probability = self.randomVariableDistributionModel.probability(x.label)
			label = x.label
			probabilityClasses = []
			for c in self.probabilityObjectForEachClass:
				cLabel = c.label
				cProbability = x.distributionModelClass.probability(c.label)
				probabilityClasses.append(ProbabilityClass(cLabel, cProbability))

			randomVariables.append(RandomVariable(label, probability, probabilityClasses))
		
		return Classifier(randomVariables)

	def __createAccumulator(self):
		accLabels = []

		for x in self.classifierVariables:
			xLabel = x.label
			yLabels = []
			yClasses = x.getAllClasses()
			
			for y in yClasses:
				yLabels.append(y.label)

			accLabels.append(AccumulatorLabel(xLabel, yLabels))

		return AccumulatorProbabilityCalculator(accLabels)

	def __calculateAccumulator(self, sampleNumber):
		accumulator = self.__createAccumulator()

		for i in range(0, sampleNumber):
			xLabel = self.randomVariableDistributionModel.sample()
			index = next(index for index, value in enumerate(self.classifierVariables) if value.label == xLabel)
			yLabel = self.classifierVariables[index].sampleAClass()

			accumulator.accumulate(xLabel, yLabel)

		return accumulator


	def generateClassifier(self, sampleNumber):
		accumulator = self.__calculateAccumulator(sampleNumber)
		randomVariables = []

		for x in self.classifierVariables:
			xLabel = x.label
			xProbability = accumulator.probability(xLabel)
			probabilityClasses = []

			for c in self.probabilityObjectForEachClass:
				cLabel = c.label
				cProbability = accumulator.jointProbability(xLabel, cLabel)
				probabilityClasses.append(ProbabilityClass(cLabel, cProbability))

			randomVariables.append(RandomVariable(xLabel, xProbability, probabilityClasses))

		return Classifier(randomVariables)


	def minNumberOfSamples(self, epsilon, delta, H):
		if self.__isClassesDistributionModelDeterministic():
			return self.__minNumberOfSamplesDesterministicModel(delta, epsilon, H)
		else:
			return self.__minNumberOfSamplesNonDeterministicModel(delta, epsilon, H)

	def __minNumberOfSamplesDesterministicModel(self, epsilon, delta, H):
		return math.ceil((1.0/epsilon) * math.log(float(H) / delta))

	def __minNumberOfSamplesNonDeterministicModel(self, epsilon, delta, H):
		return math.ceil((1.0/(2* (epsilon**2))) * math.log((2*float(H)) / delta))

	def __isClassesDistributionModelDeterministic(self):
		for x in self.classifierVariables:
			if not x.isClassesModelDeterministic():
				return False

		return True

