from RestrictionControl.ErrorMeasurer import ErrorMeasurer, ConditionalProbabilityErrorMeasurer
import math

class RestrictionControl:
	def __init__(self, errorMeasurer, classifierGenerator, spaceCardinality, delta=0.1, epsilon=0.1, numberOfTries=100):
		self.__classifierGenerator = classifierGenerator
		self.optClassifier = self.__classifierGenerator.generateOptimumClassifier() 
		self.delta = delta
		self.epsilon = epsilon
		self.errorMeasurer = errorMeasurer
		self.numberOfTries = numberOfTries
		self.spaceCardinality = spaceCardinality		
	
	def getVariation(self):
		return self.__classifierGenerator.getVariation()

	def getOptimumClassifier(self):
		return self.optClassifier

	def getMinNumberOfSamples(self, epsilon, delta, spaceCardinality):
		return self.__classifierGenerator.minNumberOfSamples(epsilon, delta, spaceCardinality)

	def hasPassed(self, samplesNumber):		
		deltaProbability = self.__timesPassedByEpsilonCriteria(samplesNumber)
		comparableDelta = 1.0 - self.delta
		
		print(str(samplesNumber) + " - delta = "  +  str(comparableDelta) + " classifier delta = " + str(deltaProbability))

		return deltaProbability > comparableDelta

		
	def __timesPassedByEpsilonCriteria(self, samplesNumber):
		count = 0

		for i in range(1, self.numberOfTries):
			estimatedClassifier = self.__classifierGenerator.generateClassifier(samplesNumber)
			deltaError = self.errorMeasurer.deltaError(estimatedClassifier, self.optClassifier)
			
			if deltaError <= self.epsilon:
				count += 1		

		return float(count) / float(self.numberOfTries)