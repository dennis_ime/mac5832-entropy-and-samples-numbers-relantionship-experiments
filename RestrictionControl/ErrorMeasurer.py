from abc import ABCMeta, abstractmethod
import math

class ErrorMeasurer:
	__metaclass__ = ABCMeta
	
	@abstractmethod
	def errorOnX(self, classifierVariable, optClassisierVariable):
		pass

	@abstractmethod
	def classifierMeanError(self, classifier, optimumClassifier):
		pass

	@abstractmethod
	def deltaError(self, colassifier, optimumClassifier):
		pass

	def _pickAClassOnTheClassifier(self, classifierVariable, classToLookFor):
		for c in classifierVariable.probabilityClasses:
			if c.label == classToLookFor.label:
				return c

		raise NameError("Not found class")

class SimpleMeanErrorCalculator(ErrorMeasurer):
	__metaclass__ = ABCMeta
	
	@abstractmethod
	def errorOnX(self, classifierVariable, optClassisierVariable):
		pass

	def classifierMeanError(self, classifier, optimumClassifier):
		sumError = 0.0
		for i in range(0, len(classifier.randomVariables)):
			error = self.errorOnX(classifier.randomVariables[i], optimumClassifier.randomVariables[i])
			sumError += (error * optimumClassifier.randomVariables[i].variableProbability)

		return sumError

	
	def deltaError(self, colassifier, optimumClassifier):
		classifierError = self.classifierMeanError(classifier, optimumClassifier)
		optimumClassifierError = self.classifierMeanError(optimumClassifier, optimumClassifier)

		return math.fabs(optimumClassifierError - classifierError)

class ConditionalProbabilityWithOptClassifierClassErrorMeasurer(SimpleMeanErrorCalculator):

	def errorOnX(self, classifierVariable, optClassisierVariable):
		maxOptProbabilityClass = optClassisierVariable.pickUpMaxProbabilityVariableClass()
		probabilityClass = self._pickAClassOnTheClassifier(classifierVariable, maxOptProbabilityClass)

		return 1 - probabilityClass.probability


class ConditionalProbabilityErrorMeasurer(ErrorMeasurer):
	def errorOnX(self, classifierVariable, optClassisierVariable):
		maxProbabilityClass = classifierVariable.pickUpMaxProbabilityVariableClass()

		return 1 - maxProbabilityClass.probability

	def classifierMeanError(self, classifier, optimumClassifier):
		sumError = 0.0
		for i in range(0, len(classifier.randomVariables)):
			error = self.errorOnX(classifier.randomVariables[i], optimumClassifier.randomVariables[i])
			sumError += (error * optimumClassifier.randomVariables[i].variableProbability)

		return sumError

	def deltaErrorOnX(self, classifierVariable, optClassifierVariable):
		optProbabilityClass = optClassifierVariable.pickUpMaxProbabilityVariableClass()				
		maxProbabilityClass = classifierVariable.pickUpMaxProbabilityVariableClass()				

		
		if (maxProbabilityClass.label != optProbabilityClass.label):			
			return optClassifierVariable.variableProbability * math.fabs(optProbabilityClass.probability - maxProbabilityClass.probability)
		else:
			return 0.0



	def deltaError(self, classifier, optimumClassifier):
		sumError = 0.0 

		for i in range(0, len(classifier.randomVariables)):			
			sumError += self.deltaErrorOnX(classifier.randomVariables[i], optimumClassifier.randomVariables[i])

		#print(sumError)
		return sumError
		

#Barrera recomendation -- The Label must be a number
class SameClassErrorMeasurer(SimpleMeanErrorCalculator):

	def errorOnX(self, classifierVariable, optClassisierVariable):
		maxOptProbabilityClass = optClassisierVariable.pickUpMaxProbabilityVariableClass()
		probabilityClass = self._pickAClassOnTheClassifier(classifierVariable, maxOptProbabilityClass)

		return abs(maxOptProbabilityClass.probability - probabilityClass.probability)

#Optimal Mean Absolute Error
class OptimalMAEErrorMeasurer(ErrorMeasurer):
	def errorOnX(self, classifierVariable, optClassisierVariable):
		if classifierVariable.variableClassLabel == optClassisierVariable.variableClassLabel:
			return 0.0
		else:
			return 1.0

	def classifierMeanError(self, classifier, optimumClassifier):
		sumError = 0.0
		for i in range(0, len(classifier.randomVariables)):
			error = self.errorOnX(classifier.randomVariables[i], optimumClassifier.randomVariables[i])
			sumError += (error * optimumClassifier.randomVariables[i].variableProbability)

		return sumError

	
	def deltaError(self, classifier, optimumClassifier):		
		return self.classifierMeanError(classifier, optimumClassifier)
 

#After Professor Ronaldo review
class ConditionalProbabilityWithOptClassifierClassUsingDistributionModelErrorMeasurer(ErrorMeasurer):
	def errorOnX(self, classifierVariable, optClassisierVariable):
		maxProbabilityClass = classifierVariable.pickUpMaxProbabilityVariableClass()
		probabilityClass = self._pickAClassOnTheClassifier(optClassisierVariable, maxProbabilityClass)

		return 1 - probabilityClass.probability

	def classifierMeanError(self, classifier, optimumClassifier):
		sumError = 0.0
		for i in range(0, len(classifier.randomVariables)):
			error = self.errorOnX(classifier.randomVariables[i], optimumClassifier.randomVariables[i])
			sumError += (error * optimumClassifier.randomVariables[i].variableProbability)

		return sumError

	def deltaErrorOnX(self, classifierVariable, optClassifierVariable):
		optProbabilityClass = optClassifierVariable.pickUpMaxProbabilityVariableClass()				
		classToLookFor = classifierVariable.pickUpMaxProbabilityVariableClass()				
		maxProbabilityClass = self._pickAClassOnTheClassifier(optClassifierVariable, classToLookFor)

		
		if (maxProbabilityClass.label != optProbabilityClass.label):			
			return optClassifierVariable.variableProbability * math.fabs(optProbabilityClass.probability - maxProbabilityClass.probability)
		else:
			return 0.0



	def deltaError(self, classifier, optimumClassifier):
		sumError = 0.0 

		for i in range(0, len(classifier.randomVariables)):			
			sumError += self.deltaErrorOnX(classifier.randomVariables[i], optimumClassifier.randomVariables[i])


		return sumError


class RiskErrorMeasurer(ErrorMeasurer):
	
	def errorOnX(self, classifierVariable, optClassisierVariable):
		probabilityClass = classifierVariable.pickUpMaxProbabilityVariableClass()	

		return (1 - probabilityClass.probability)  * classifierVariable.variableProbability;
	
	def classifierMeanError(self, classifier, optimumClassifier):
		sumError = 0.0

		for rv in classifier.randomVariables:
			sumError += self.errorOnX(rv, None)

		return sumError
	
	def deltaError(self, classifier, optimumClassifier):
		classifierMeanError = self.classifierMeanError(classifier, None)
		optClassifierMeanError = self.classifierMeanError(optimumClassifier, None)

		return math.fabs(optClassifierMeanError - classifierMeanError)