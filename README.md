Programa feito para a disciplina de Aprendizagem Computacional da pós-graduação do IME-USP.

Para sua execução é necessários dois parâmetros um arquivo de configuração em JSON (veja modelo na pasta 'jsonSchema' e alguns dos arquivos usados no projeto na pasta 'config') e uma pasta onde será gerado os resultados.


para rodar utilize:

python MinSampleByEntropyTester.py -c <arquivo de configuração> -o <pasta onde será gerado arquivos de saída>