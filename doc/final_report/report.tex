\documentclass[a4paper,10pt]{report}
\usepackage[brazil]{babel}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{algorithm}
\usepackage{algpseudocode} 

% Title Page
\title{Estudo da relação da entropia de um classificador com o número mínimo de amostras para satisfabilidade dos critérios $\epsilon$ e $\delta$}
\author{Dennis Jose da Silva, NUSP 9176517, dennis@ime.usp.br
\and Henrique Toledo Coelho, NUSP 5377917, htcoelho@grad.icmc.usp.br
\and Prof. Dr. Ronaldo  Fumio Hashimoto, Orientador
}


\begin{document}
\maketitle

\chapter{Introdução}
\section{Algoritmo de aprendizagem de um classificador}
\label{sec:sec1}

O algoritmo de aprendizagem é um importante objeto de estudo na área de aprendizagem computacional,
que tem como objetivo descrever um modelo que consegue aprender determinada função a partir de um conjuto de 
exemplos em um processo conhecido como treinamento (figura~\ref{fig:fig1}). Dentro dessas funções que um algoritmo de aprendizagem pode
aprender, a classificação vem empenhando um importante papel na área.

\begin{figure}[hb]

\includegraphics[width=3.0in]{./img/learningAlgorithmModel.png}
\centering
\caption{modelo de um algoritmo de apredizagem, onde um conjunto de amostras é utilizado para aprender
uma função \label{fig:fig1}}
\end{figure}

Para que seja possível a utilização de um algoritmo de apredizagem para solucionar um problema é necessário que as amostras
sigam algum modelo de distribuição, que é desconhecido. Caso contrário, não seria necessário aplicar um algoritmo de 
apredizagem, já que o objetivo do algoritmo aplicado é aproximar-se o máximo possivel da distribuição que gerou as amostras.

\pagebreak

Portanto, é possivel descrever um classificador por meio de uma distribuição conjunta estimada a partir do número das amostras
geradas pela fonte. Essa distribuição normalmente é descrita por meio de uma tabela de distribuição conjunta:

\begin{table}[h]
\centering
\begin{tabular}{|r|r|r|r|r|r|r|}
\hline
$p(x)$&$x$&$p(y_1|x)$&$p(y_2|x)$&...&$p(y_n|x)$&$\hat{\psi}(x)$\\
\hline
$p(x_1)$&$x_1$&$p(y_1|x_1)$&$p(y_1|x_2)$&.&$p(y_1|x_n)$&$\hat{\psi}(x_1)$\\
$p(x_2)$&$x_2$&$p(y_2|x_1)$&$p(y_1|x_2)$&.&$p(y_2|x_n)$&$\hat{\psi}(x_2)$\\
.& . & . & . & . & . & . \\
.& . & . & . & . & . & . \\
.& . & . & . & . & . & . \\
$p(x_n)$&$x_n$&$p(y_n, x_1)$&$p(y_1, x_2)$&.&$p(y_n, x_n)$&$\hat{\psi}(x_n)$\\
\hline
\end{tabular} 	
\caption{tabela de probabilidade conjunta, utilizado por um classificador $\hat{\psi}$ \label{tab:tab1}}

\end{table}

O objetivo do classificador é encontrar a classe de uma determinada variável com o menor erro possível. Isso é dado analisando 
a classe com a maior probabilidade possível:

\[ {\psi}(x) = \arg \max_{y \in Y} p(y=y|x) \]

Se a probabilidade de um classificador acertar a classe de uma variável aleátoria é a probabilidade da classe com maior probabilidade
dessa variável, a probabilidade do classificador errar sua classificação é a probabilidade da variável aleátoria ser de qualquer outra 
classe que não essa de maior probabilidade, ou seja, o erro de um classificador classificar uma variável aleátoria é a probabilidade
da variável aleátoria não ser da classe que o classificador esperava.

\[ Er(\psi(x)) = 1 - \max_{y \in Y} p(y=y|x) \]   

A partir da equação de erro do classificador em uma variável aleátoria $x$, é possível formular uma equação de erro médio de um classificador
${\psi}$.

\[ Er(\psi) = \sum_{x \in X} (1 - \max_{y \in Y} p(y=y|x)) p(x)\]

Nota-se que o erro de um classificador qualquer está relacionado com a probabilidade das classes de maior probabilidade de suas 
variáveis aleátorias, ou seja, o erro de um classificador qualquer está diretamente relacionado à 
entropia (medida de massa de probabilidade, será discutida na seção ~\ref{sec:sec3}) de seus modelos de distribuição.

\section{Critério de qualidade $\epsilon$ e $\delta$ e o número mínimo de amostras}
\label{sec:sec2}

Supondo que o classificador ótimo $\psi_{opt}$, ou seja, o classifcador que tem a menor taxa de risco (definido por uma função $R$ qualquer) fosse conhecido é esperado que o 
classificador estimado que possua o risco mais próximo possível desse classifcador ótimo, seja o melhor classificador gerado por um 
procedimento de treinamento. Portanto é preferível que a diferença de risco do classificador ótimo com o melhor classificador estimado
seja próximo de zero. Conhecendo o problema de apredizagem é possível escolher um valor $\epsilon$ aceitável que mede a distância 
do classificador estimado ao classificador ótimo, portanto definindo esse valor $\epsilon$, é desejavel que cada classificador estimado gerado para esse modelo possua um valor de diferença do classificador ótimo abaixo desse $\epsilon$:

\[ |R(\psi_{opt}) - R(\hat{\psi})| < \epsilon \]


Conhecendo qual a distância do classificador estimado ao classificador ótimo é possível calcular qual probabilidade do modelo gerar classificadores abaixo do parâmetro $\epsilon$:

\[ Pr(|R(\psi_{opt}) - R(\hat{\psi})| < \epsilon ) \]  

Com esse dado pode-se introduzir a variável $\delta$ que mede o desempenho do modelo em gerar classificadores estimados próximos ao classificador ótimo.

\[ Pr(|R(\psi_{opt}) - R(\hat{\psi})| < \epsilon ) > 1 - \delta \] 

A equação acima define as restrições necessárias para medição da qualidade de um modelo com referência a sua capacidade de gerar boas estimações. Nota-se que, quanto menor o $\delta$ mais restrito o classificador deve ser (estimações mais semelhantes ao classificador ótimo).

A partir das variáveis $\epsilon$ e $\delta$ foi formulado a equação que define o número mínimo de amostras necessárias para que o classificador satisfaça a restrição definida pelas mesmas. Existem duas fórmulas, uma para o modelo determinístico, ou seja, sabe-se a priori a qual classe a variável aleátoria pertence ($p(x|y_i)=1$ e $p(x|y_{j\neq i }) = 0)$:

\[ m(\epsilon, \delta) = \frac{1}{\epsilon} ln(\frac{|H|}{\delta}) \]

E para problemas não determinísticos, ou seja, não se sabe a priori a qual classe uma dada variável aleátoria pertece ($p(x|y) \neq 1$): 

\[ m(\epsilon, \delta) = \frac{1}{2 \epsilon^2} ln(\frac{2 |H|}{\delta}) \]

A variável $|H|$ é a cardinalidade do espaço de classificadores que normalmente é um número muito grande, pois o mesmo depende do número de variáveis aleátorias e de seu dominío.

\section{Entropia}
\label{sec:sec3}

A Entropia é uma medida definida por C. Shannon e N. Wiener, utilizada em teoria da informação e que mede a quantidade de informação de uma mensagem. A quantidade de informação em teoria da informação é a medida de incerteza de uma determinada mensagem ser recebida, logo conclui-se que quanto menor a chance de uma mensagem ser recebida maior é sua informação, e que uma mensagem já conhecida pelo receptor não transmite informação alguma. 

C. shannon e N. Wiener definem a medida de incerteza ou entropia como:

\[ H = -\sum_{i=1}^n p_i lg(p_i) \]

A entropia possui três propriedades que são importantes para a análise de classificadores:

\begin{enumerate}
	\item A Entropia máxima é dada quando é calculada em um modelo de distribuição uniforme e a mínima quando é calculada em modelo dsterminístico.
	\item O gráfico da entropia é simétrico no ponto que representa a distribuição uniforme.
	\item A entropia é representada por uma curva monótona entre o modelo uniforme e o determinístico.
\end{enumerate}

Essas três propriedades podem ser fácilmente observadas pelo gráfico do cálculo da entropia para o estudo da distribuição de probabilidade do lançamento de uma moeda, definida por (p(x), 1 - p(x)) (figura~\ref{fig:fig2}):

\begin{figure}[ht]

\includegraphics[width=3.5in]{./img/EntropyGraph.png}
\centering
\caption{Gráfico da entropia de modelos de distribuição de probabilidade para o lançamento de uma moeda (p(x), 1- p(x))\label{fig:fig2}}
\end{figure}

Nota-se no gráfico que o nos pontos 0.0 e 1.0 da abscissa a entropia é zero, ou seja, a entropia é mínima nos pontos que representam o modelo de distribuição deterministico. Nota-se também que o ponto máximo de entropia está no ponto 0.5 da abscissa, o que representa o modelo uniforme.
Analisando o gráfico ainda é fácil notar que ele é simetrico no ponto 0.5 da abscissa e que existe uma curva monótona entre os pontos 0 e 0.5 do eixo das abscissas e entre os pontos 1.0 e 0.5.

Sabendo que o melhor classificador é aquele que erra menos e que o classificador que possui a maior massa em uma determinada classe, em média, possui menos chances de errar, a medida de entropia se apresenta um importante indicador a ser analisado para a escolha de um determinado classificador. Dado que, quanto maior a entropia mais distribuido as probabilidades estão, busca-se o classificador que possua a menor entropia média. Portanto busca-se o classificador que minimize a seguinte expressão:

\[
	\overline{H} = \sum_{x \in X}H(y|x) p(x)
\]

A equação da entropia média de um classificador é a soma da entropia de cada modelo de distribuição, associado às variáveis aleátorias do modelo a ser aprendido, utilizando a probabilidade de ocorrer essa variável aleátoria como peso da entropia de seu modelo.

\section{Estudo da relação da entropia de um classificador e número mínimo de amostras}
\label{sec:sec4}

Se a entropia (apresentada na seção ~\ref{sec:sec2}) mede a massa de distribuição de probabilidade, e a taxa de erro está relacionada entre a diferença da maior probabilidade da classe de uma variável aleátoria, consequentemente quanto menor a entropia (massa de probabilidades mais concentrada) menor será a taxa de erro (existirá uma classe para a variável aleátoria muito mais provável que as outras classes).

Sabendo da relação da taxa de erro com a entropia de um classificador, intuitivamente é possivel inferir que a entropia também está relacionada ao número mínimo de amostras que satisfazem o critério $\epsilon$ e $\delta$ apresentado na seção ~\ref{sec:sec2}. Essa foi uma intuição identificada pelo professor Dr. Junior Barerra e apresentada pelo professor Dr. Ronaldo Fumio Hashimoto.

A proposta do projeto é identificar essa relação da entropia média do classifcador com o número mínimo de amostras de maneira experimental. Para isso é proposto o desenvolvimento de um programa que crie classificadores ótimos $\psi_{opt}$ de diferentes entropias médias. E, desse classifcador, serão gerados diversos classificadores estimados $\hat{\psi}$, utilizando um números de amostras crescente até que o critério $\epsilon$ e $\delta$ seja atingido. Ao final do programa é esperado a geração de um gráfico que relacione a entropia dos classificadores e o número de amostras que satisfazem o críterio de qualidade $\epsilon$ e $\delta$.

\chapter{Desenvolvimento}

\section{modelo}
\label{sec:sec5}
Para esse problema foi identificado que um bom modelo deveria ter uma distribuição fácil de calcular nas variáveis aleátorias e que deveria ter um modelo de distribuição que fosse capaz de sofrer uma variação na entropia média de uma maneira parametrizada nas classes. Dadas essas características foram escolhidos o modelo uniforme para as variáveis aleátoria, e uma adaptação do modelo Gaussiano para as classes (devido a esse modelo ter seu domínio no conjunto dos números reais).

O modelo uniforme foi escolhido por que é um modelo bem simples e de cálculo fácil, que não impactaria no resultado final.

\[ p(x) = \frac{x}{n}, \forall x \in \{x_1, x_2, x_3, ... , x_n\} \]

O modelo Gaussiano foi utilizado porque possui o parâmetro $\sigma$, que pode ser utilizado para variar a entropia da distribuição. A definição do modelo Gaussiano é dada pelo cálculo de sua função de distribuição de probabilidade (que possui sigla PDF, do inglês \textit{probability distribution function}). Essa função recebe dois parâmetros que servem para definir a média ($\mu$, ponto com maior distribuição de massa de probabilidade) e o desvio padrão ($\sigma$ que define a distribuição da massa de probabilidade em torno da média).

\[  pdf(x, \mu, \sigma ) = \frac{1}{\sqrt{2 \pi} \sigma} e^{-\frac{1}{2} \left(\frac{x - \mu}{\sigma}\right)^2} \]

Porém o modelo Gaussiano é definido no conjunto dos números reais, onde a área abaixo da curva gerada por sua $pdf$ dentro de um intervalo é a probabilidade de algum número dentro daquele intervalo ocorrer. Porém como o classificador estudado possuí classes discretas essa abordagem não poderia ser utilizada. Para contornar essa incompatibilidade, em vez de utilizar a área da curva em um determinado intervalo, foi utilizado a função $pdf$ daquele ponto específico normalizado. Portanto a função de proababilidade utilizada no projeto foi:

\[ n = \sum_{x \in X}pdf(x), \:\:\:\:\:\:   p(x) = \frac{pdf(x)}{n} \]

O modelo Gaussiano foi importante nesse projeto por causa de seu parâmetro $\sigma$ que define como as probabilidades estão distribuidas em torno da média $\mu$, conforme mostrado no gráfico a seguir:

\begin{figure}[ht]
\includegraphics[width=3.5in]{./img/gaussian_variation.png}
\centering
\caption{Gráfico que mostra o comportamento da função $pdf$ do modelo Gaussiano em relação a variação do parâmetro $\sigma$.
\label{fig:fig3}}
\end{figure}

Na prática, o software desenvolvido foi configurado com alguns $\sigma$'s diferentes que geravam classificadores de diferentes entropias e plotava em um gráfico.

\section{Implementação do software de experimentos}
\label{sec:sec6}

Para esse estudo foi desenvolvido um programa em linguagem Python, que recebe como entrada um arquivo de configuração, onde são definidas as variáveis a serem utilizadas, os modelos de distribuição e seus parâmetros, as variáveis $\epsilon$ e $\delta$, a cardinalidade do espaço, a quantidade de classificadores estimados a serem utilizados no cálculo de restrição $\epsilon$ e $\delta$, o metódo utilizado para o cálculo do erro e limites para a parada da execução do algoritmo. Todas essas variáveis podem ser customizadas em um arquivo de configuração do formato JSON, que pode ser consultado no repósitorio de código-fonte do projeto. E como saída é gerado uma página HTML, com os números de amostras calculadas analicamente, um gráfico relacionando a entropia média dos classificadores com o número minimo de amostras que cumpriu o critério $\epsilon$ e $\delta$, os pontos utilizados para a plotagem do gráfico e os classificadores ótimos.

Para os experimentos feitos no projeto, foram utilizados os seguintes parâmetros:

\begin{itemize}
 
 \item Foram definidos que o conjunto de variáveis aleátorias seria o domínio dos números binários de 4 digitos e as classes seriam os números de 1 a 5: $\psi: \{0,1\}^4 \rightarrow \{1, 2, 3, 4, 5\}$;
 
 \item Dois principais modelos de distribuição, o Uniforme para as variáveis aleátorias $x$ e o Gaussiano para para as classes $y$ do classificador;
 
 \item Os modelos Gaussianos eram parametrizados com o valor de média $\mu = 3$ e desvio padrão $\sigma$ variando entre 0,1 e 1,37237237237, valores que geravam entropias entre zero e 2,2 em incrementos de 0,1;
 
 \item $\delta = 0.1$ e $\epsilon = 0.1$;
 
 \item Eram gerados 25 classificadores estimados para a verificação dos críterios $\epsilon$ e $\delta$;
 
 \item A cardinalidade era 152587890625. Calculada a partir de $expoente = 2^4 = 16$ e $|H| = 5^{expoente} = |H| = 5^{16} |H| = 152587890625$;
 
 \item O cálculo de erro do classificador utilizado na fórmula de satisfabilidade dos críterios $\epsilon$ e $\delta$ foi simplificado para cálculo da diferença dos erros. Na prática, a fórmula $|R(\psi_{opt}) - R(\hat{\psi})|$ foi simplificada para $\Delta erro = \sum_{x: \psi_{opt}(x) \not= \hat{\psi}(x)}  (p(y = \psi_{opt}(x)| x) - \hat{p}(y = \hat{\psi}(x)) p(x)$.
 
\end{itemize}

Após receber o arquivo de configuração, o programa converte esses parâmetros em um formato que pode ser utilizado por ele. Ele então calcula o classificador ótimo analicamente, calculando a proababilidade em suas variáveis por meio da PDF (\textit{Probability density function}) de seu modelo. Ele então começa a gerar os classificadores estimados, começando com 1 amostra e incrementado esse valor até que o mesmo satisfaça os críterios $\epsilon$ e $\delta$. Para gerar os classificadores estimados o programa gera amostras que obedeçam os modelos de distribuição definidos no arquivo de configuração e calculadas como classificador ótimo. A partir dessas amostras, ele calcula suas probabilidades ($m$ = número de amostras, $p(x_i) = \#(x_i, y) / m$ e $p(x_i|y_i) = \#(x_i, y_j)/\#(x_i, y)$) e com esses valores calculados então é verificado se o mesmo satisfaz as restrições $\epsilon$ e $\delta$.

Para a geração das amostras foi utilizada a técnica de probabilidade acumulada. Isso foi necessário por causa da natureza da função aleátoria do próprio Python que gerava um número aleátorio entre 0 e 1 a cada chamada. Para incluir a probabilidade da amostragem foram criados intervalos entre $p(x_i)$ e $p(x_{i + 1})$. E quando a amostra era gerada em um desses intervalos, o rótulo do intervalo era atribuido como sua classe:

\begin{figure}[ht]
\includegraphics[width=4.0in]{./img/AcumulativeProbabilities_example.jpg}
\centering
\caption{Demostração do metódo das probabilidades acumulativas e seus intervalos, para que um número aleátorio entre 0 e 1 corresponda a uma classe do modelo obdecendo um modelo de probabilidade.
\label{fig:fig4}}
\end{figure}

Após ele concluir os cálculos do primeiro classificador definido no arquivo de configuração ele passa para o próximo e assim sucessivamente. Para cada modelo que o programa encontra o número minímo de amostras ele armazena esse número juntamente com a entropia média calculada, e ao concluir todos os modelos ele gera uma tabela HTML com as tabelas de probabilidade conjunta de cada classificador ótimo, sua entropia média, o número de amostras e o desvio padrão utilizado, assim como um gráfico, onde os pontos plotados representam a quantidade de amostras utilizadas para satisfazer os critérios $\epsilon$ e $\delta$ e a entropia média do modelo.

\section{Resultados}
\label{sec:sec7}


O resultado obtido em um determinado fator foi como o esperado, ou seja, conforme a entropia média dos classificadores aumentava, mais amostras eram necessárias para que os critérios $\epsilon$ e $\delta$ fossem satisfeitos. Em valores com entropias muito baixas o número de amostras foi muito mais baixo do que o calculado pela fórmula analítica. Porém era esperado que o número de amostras convergisse para o número calculado analiticamente (1438 no caso dos experimentos feitos no projeto), o que não ocorreu. O número mais alto obtido foi 287 amostras, obtidas nos 2 primeiros experimentos para um classificador com valor 2.32000443624 para sua entropia média (maior entropia utilizada nos experimentos) e 281 de média conforme a plotagem abaixo exibe:

\begin{figure}[ht]
\includegraphics[width=4.5in]{./img/all_plotting.png}
\centering
\caption{Plotagem consolidada dos resultados obtidos nos experimentos, com a plotagem de cada experimento e gráfico da média dos 3 experimentos executados no projeto.
\label{fig:fig5}}
\end{figure}


Analisando o gráfico é possivel notar uma curva acentuada quando a entropia média chega perto de 1.5. O número de amostras tem um crescimento muito maior do que ele apresentava antes de 1.5, mostrando que quanto maior a entropia do modelo, maior é o efeito da mesma no desempenho do classificador e na quantidade de amostras necessárias para a satisfabilidade dos critérios $\epsilon$ e $\delta$.

A partir dos resultados, conclui-se que, aparentemente, a entropia é uma caracteristica do modelo que pode impactar no desempenho da apredizagem e que o número de amostras depende não apenas das variáveis $\epsilon$ e $\delta$ mas também da entropia do modelo utilizada no desenho das classes. Conclui-se também que quanto menor a entropia média das classes do classificador, menor será a quantidade de amostras necessárias para que o mesmo satisfaça os critérios $\epsilon$ e $\delta$. Além disso, os experimentos reafirmam a entropia como uma característica importante a ser analisada no momento de desenvolvimento do modelo de aprendizagem.


\chapter{Trabalhos Futuros}

A partir dos resultados desse projeto surgem algumas questões encontradas no decorrer de seu desenvolvimento. Surgem também perguntas da análise dos resultados, que podem ser pontos de partida para novos trabalhos. Questões como as mostradas abaixo:

\begin{itemize}
 \item Por que os experimentos, mesmo com entropia média alta não convergiram para o número minímo de amostras? Para isso, seria necessárias estudar a fórmula analítica mais a fundo e verificar como funciona a inequação de Hoeffding, que é a origem dessa equação;
 
 \item Analisar a fórmula do "$\Delta erro$" utilizada no projeto. A mesma causou bastante discussão entre as pessoas envolvidas e ainda assim não ficou claro se a fórmula utilizada é a que melhor descreve a diferença entre o classificador ótimo e o estimado necessária para o cálculo da satisfabilidade dos critérios $\epsilon$ e $\delta$;
 
 \item Realizar experimentos com diferentes valores de $\epsilon$ e $\delta$ e verificar se as propriedades são mantidas (menor entropia, menor número de amostras);
 
 \item Realizar experimentos com um número maior de classificadores estimados para a verificação da sastisfabilidade dos critérios $\epsilon$ e $\delta$. Para os experimentos desse projeto, foram utilizados apenas 25 classificadores estimados para cada verificação da sastisfabilidade.
 
\end{itemize}


\chapter{Apêndices}
\section{Links do projeto}
\label{sec:sec9}

Código fonte do projeto está em um repositório público no site bitbucket e o mesmo pode ser acessado pelo link:  

\begin{verbatim}
https://bitbucket.org/dennis_ime/ 
mac5832-entropy-and-samples-numbers-relantionship-experiments 
\end{verbatim}




Os resultados obtidos pelos experimentos com as tabelas dos classificadores ótimos e o gráfico de cada experimento em detalhe podem ser acessados pelo link: 

\begin{verbatim}
http://www.ime.usp.br/~dennis/machine-learning/projeto/ 
conditionalProbability_using_estimatedProbability/
\end{verbatim}

\pagebreak

\section{Diagrama de execução}
\label{sec:sec11}

Abaixo segue um diagrama que exibe os procedimentos do programa em uma versão simplificada. A seção ~\ref{sec:sec11} apresenta o pseudocódigo do código com mais detalhes da implementação do software, para mais deltalhes é possível acessar o repósitorio com o código fonte do programa por meio do link na seção ~\ref{sec:sec9}.

\begin{figure}[ht]
\includegraphics[width=5.0in]{./img/diagram.jpg}
\centering
\caption{Diagrama de execução do programa simplificado
\label{fig:fig6}}
\end{figure}



\section{Pseudocódigo Completo}
\label{sec:sec10}

Abaixo segue o pseudocódigo do programa completo:

\begin{algorithm}[ht]
\caption{Execução de experimentos}
\label{alg1}

\begin{algorithmic}

\Function {main}{$config$, $output$}
  \State experiments $\gets$ \Call{parse}{$config$}
  \State points $\gets$ \Call{run}{$experiments$}
  \State \Call{plot}{$points$, $output$}
\EndFunction

\\

\Function {run}{$experiments$}
  \State $result \gets$ lista vazia
  \ForAll {$experiment$ in $experiments$}
    \State $optClassifier$ = \Call{generateOptimumClassifier}{experiment}
    \State $meanEntropy \gets$ \Call{meanEntropy}{$optClassifier$}
    \State $minSamples \gets$ \Call{runExperiment}{$optClassifier$, $experiment$}
    
    \State \Call{append}{$result$, $(meanEntropy, minSamples)$}
  \EndFor
  
  \State \Return $result$
\EndFunction

\\

\Function {runExperiment}{$optClassifier$, $experiment$}
  \State $minSamples \gets$ 1 
  \State $oc \gets optClassifier$
  \State $e \gets experiment$
  
  
  \While {$minSamples \leq e.upperBound$}
    \State $hasPassed \gets$ \Call{IsCriteriaSatisfied}{$oc$, $e$, $minSamples$}
    \If {$hasPassed$}
      \State $passCount \gets$ 1
      \While{True}
	\If {$passCount \geq experiment.triesToPass$}
	  \State 
	  \Return $minSamples$
	\Else
	  \State $s \gets minSamples + passCount$
	  \State $hasPassed \gets$ \Call{IsCriteriaSatisfied}{$oc$, $e$, $s$}
	  \If{$hasPassed$}
	    \State $passCount \gets passCount + 1$ 
	  \Else
	    \State $minSamples \gets passCount - 1$
	    \State Break
	  \EndIf
	\EndIf
	
      \EndWhile
    \EndIf
    \State $minSamples \gets minSamples + 1$
  \EndWhile
  
  \State \Return $minSamples$
\EndFunction

\end{algorithmic}
\end{algorithm}
  

\begin{algorithm}[ht]
\caption{Geradores de classificadores}
\label{alg2}

\begin{algorithmic}

\Function {generateOptimumClassifier}{$experiment$}
\State $xs \gets $ lista vazia

\ForAll{$var$ in $experiment.randomVariables$}
  \State $px \gets$ \Call{probability}{experiment.distModel, var}
  \State $xLabel \gets var.label$
  \State $classes \gets$ lista vazia
  
  \ForAll {$vc$ in $var.classes$}
    \State $pyx \gets $ \Call{probability}{$vc.distModel$, $vc$}
    \State \Call{append}{$classes$, $(vc.label, pyx)$}
  \EndFor
  
  \Call{append}{$xs$, $(px, xlabel, classes)$}
\EndFor

\State \Return $xs$
\EndFunction

\\
\Function{estimateClassifier}{$numSamples$, $experiment$}
\State $e \gets experiment$ 
\State $samples \gets$ lista vazia
\State $xs \gets$ lista vazia

\For {$\:\:i \gets 0$ upto $numSamples$}
  \State $x \gets $ \Call{sample}{$e.distMode$, $e.vars$}
  \State $y \gets $ \Call{sample}{$x.distMode$, $x.classes$}
  
  \State \Call{append}{$samples$, $(x.label, y.label)$}
\EndFor



\ForAll {$v$ in $vars$}
  \State $filteredSamples \gets$ \Call{filterOnX}{$samples$, $v.label$}  
  \State $countXs \gets$ length of $filteredSamples$
  
  \State $ys \gets$ lista vazia
  \ForAll {$c$ in $v.classes$}
    \State $countYs \gets$ length of \Call{filterOnY}{$filteredSamples$, $c.label$}
    \State $pyx \gets$ $countYs/countXs$
    \State \Call{append}{$ys$, $(c.label, pyx)$}
  \EndFor
  
  \State $px \gets countX / $ length of $samples$
  \State \Call{append}{$xs$, $(px, v.label, ys)$}
  
\EndFor

\State \Return $xs$

\EndFunction

\end{algorithmic}
\end{algorithm}

\begin{algorithm}[ht]
\caption{Amostragem}
\label{alg3}

\begin{algorithmic}

\Function{FilterOnX}{$samples$, $label$}
  \State $filteredSamples \gets$ lista vazia
  \ForAll {$s$ in $samples$}
    \If{$s(0) == label$}
      \State \Call{append}{$filteredSamples$, $s$}
    \EndIf
   \EndFor
   
   \State \Return $filteredSamples$
\EndFunction
\\
\\
\Comment {FilterOnY é a mesma coisa que o FilterOnX mas em vez de comparar s(0) = label (label de x da dupla (x,y)) ele compara 
  s(1) = label (label de y da dupla (x,y))}
\\

\Function{sample}{$distModel$, $vars$}
\If {distModel = UNIFORM}  
  \State $index \gets$ \Call{randomInt}{0, length of $vars$}
  \State \Return $vars(index)$
\ElsIf {distModel = GAUSSIAN}
  \State $accumulators \gets$ \Call{createAccumulators}{$vars$, $distModel$}
  \State $sample \gets$ \Call{random}()
  
  \ForAll {$ac$ in $accumulators$}
    \If $\:\:ac.lowerBound \leq sample$ and $sample \leq ac.upperBound$
      \State \Return $vars(ac.index)$
    \EndIf
  \EndFor
\Else
  \State \Call{Error}{}
\EndIf

\EndFunction

\\

\Function{createAccumulators}{$vars$, $distModel$}
  \State $acs \gets$ lista vazia
  
  \For {$i \gets 0$ upto length of $vars$}
    \State $ac.index \gets i$
    \If {$i = 0$} 
      \State $ac.lowerBound \gets 0$ 
    \Else
	\State $ac.lowerBound \gets acs(i-1).upperBound$
    \EndIf
    \State $d \gets distModel$ 
    \State $ac.upperBound \gets ac.lowerBound $ + \Call{probability}{$vars(i)$, $d$} 
    \State \Call{append}{$acs$, $ac$}
    
  \EndFor
  
  \State \Return $acs$
\EndFunction

\end{algorithmic}
\end{algorithm}

\begin{algorithm}[ht]
\caption{Satisfabilidade do criterio $\epsilon$ e $\delta$}
\label{alg4}

\begin{algorithmic}
 
\Function{IsCriteriaSatisfied}{$optClassifier$, $experiment$, $minSamples$}
  \State $e \gets experiment$
  \State $oc \gets optClassifier$
  \State $m \gets minSamples$
  \State $passedOnEpsilon \gets$ \Call{timesPassedByEpsilonCriteria}{$oc$, $e$, $m$}
  \State $pr \gets passedOnEpsilon / ec.numberOfExperiments$

  \State \Return $pr \geq e.\delta$
\EndFunction

\\
 
\Function{timesPassedByEpsilonCriteria}{$optClassifier$, $experiment$, $minSamples$}
  \State $e \gets experiment$
  \State $oc \gets optClassifier$
  \State $m \gets minSamples$
  
  \State $passCount \gets 0$
  
  \For {$i \gets 1$ upto $e.numberOfExperiments$}
    \State $ec \gets$ \Call{estimateClassifier}{$m$, $e$}
    \State $\Delta erro \gets$ \Call{deltaError}{$oc$, $ec$}
    \If {$\Delta erro \leq e.\epsilon$}
      \State $passCount \gets passCount + 1$
    \EndIf
  \EndFor
  
  \State \Return $passCount$
\EndFunction

\\

\Function{getMaxProbabilityClass}{$vars$}
  \State $maxClass \gets var(0)$
  \For {$i \gets 1$ uptoc length of $vars$}
    \If{$maxClass.probability < var(i).probability$}
      \State $maxClass \gets var(i)$
    \EndIf
  \EndFor
  \State \Return $maxClass$
  
\EndFunction
\\

\Function{deltaError}{$optClassifier$, $classifier$}
  \State $sum \gets 0$
  \For {$i \gets 0$ upto length of $optClassifier$}
    \State $optClass \gets $ \Call{getMaxProbabilityClass}{$optClassifier(i).ys$}
    \State $estClass \gets $ \Call{getMaxProbabilityClass}{$classifier(i).ys$}
    
    \If {$optClass.label \not= estClass.label$}
      \State $px \gets optClassifier(i).probability$ 
      \State $sum \gets sum +  (px * |optClass.probability - estClass.probability|)$
    \EndIf
  \EndFor
  
  \State \Return $sum$
\EndFunction
 
\end{algorithmic}
\end{algorithm}


\end{document}