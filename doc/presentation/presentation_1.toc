\beamer@endinputifotherversion {3.10pt}
\select@language {brazil}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Algoritmo de Aprendizagem}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Tabela de probabilidade conjunta}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Classifica\IeC {\c c}\IeC {\~a}o}{5}{0}{1}
\beamer@subsectionintoc {1}{4}{Crit\IeC {\'e}rio $\epsilon $ e $\delta $}{6}{0}{1}
\beamer@subsectionintoc {1}{5}{Entropia e intui\IeC {\c c}\IeC {\~a}o}{7}{0}{1}
\beamer@sectionintoc {2}{Desenvolvimento}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Modelo}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Implementa\IeC {\c c}\IeC {\~a}o}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Resumo da execu\IeC {\c c}\IeC {\~a}o do programa}{10}{0}{2}
\beamer@subsectionintoc {2}{4}{Resultados - Gr\IeC {\'a}fico}{11}{0}{2}
\beamer@subsectionintoc {2}{5}{Resultados - Conclus\IeC {\~a}o}{12}{0}{2}
\beamer@sectionintoc {3}{Trabalhos Futuros}{13}{0}{3}
