from jsonschema import validate
from pprint import pprint 
import json

schemaFile = open("JsonSchema/schema.json")
inputFile = open("JsonSchema/input_example.json")

schemaDic = json.load(schemaFile)
inputDic = json.load(inputFile)

validate(inputDic, schemaDic)

print("It is validated")