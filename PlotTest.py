from Output.Ploting import CartesianSystemPlotting

plotter = CartesianSystemPlotting()
plotter.xLabel = "Entropia Media"
plotter.yLabel = "Numero minimo de amostras"
plotter.title = "Entropia x Numero minimo de amostras para epsilon=0.1 e delta=0.1"

plotter.addXYPair(0.1, 100)
plotter.addXYPair(0.2, 325)
plotter.addXYPair(0.3, 725)
plotter.addXYPair(0.4, 1000)
plotter.addXYPair(0.5, 3500)
plotter.addXYPair(0.6, 7500)
plotter.addXYPair(0.7, 10000)

plotter.plotOnImageFile("image.png")