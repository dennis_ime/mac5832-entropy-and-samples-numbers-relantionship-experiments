import math
from Output.Ploting import Point, PlottingPoint

class Experiment:
	def __init__(self, restrictionControls, triesToPass, upperBoundPercetage, minSampleInitialValue=1, minSampleStep=1):
		self.restrictionControls = restrictionControls
		self.triesToPass = triesToPass
		self.upperBoundPercetage = upperBoundPercetage
		self.minSampleInitialValue = minSampleInitialValue
		self.minSampleStep = minSampleStep

	def delta(self):
		return self.restrictionControls[0].delta

	def epsilon(self):
		return self.restrictionControls[0].epsilon

	def minSampleCalculated(self):
		return self._minSampleCalculated(self.restrictionControls[0])

	def _minSampleCalculated(self, restrictionControl):
		epsilon = restrictionControl.epsilon
		delta = restrictionControl.delta
		spaceCardinality = restrictionControl.spaceCardinality
		calculatedMinSample = restrictionControl.getMinNumberOfSamples(epsilon, delta, spaceCardinality)

		return calculatedMinSample

	def run(self):
		result = []
		print(self.restrictionControls)
		for r in self.restrictionControls:
			optClassifier = r.getOptimumClassifier()
			meanEntropy = optClassifier.meanEntropy()
			minSampleNumber = self._runClassifierTillOvercomeRestriction(optClassifier, r)

			point = Point(meanEntropy, minSampleNumber)
			plottingPoint = PlottingPoint(point, optClassifier, r.getVariation())

			result.append(plottingPoint)

		return result


	def _runClassifierTillOvercomeRestriction(self, optClassifier, restrictionControl):
		upperBound = self._calculateActualUpperBound(optClassifier, restrictionControl)
		minSampleNumber = self.minSampleInitialValue
		triesToPass = self.triesToPass



		while minSampleNumber <= upperBound:
			hasPassed = restrictionControl.hasPassed(minSampleNumber)
			if hasPassed:
				passCount = 1
				while True:
					if passCount >= triesToPass:
						return minSampleNumber
					else:
						hasPassed = restrictionControl.hasPassed(minSampleNumber + passCount)
						if hasPassed:
							passCount += 1
						else:
							minSampleNumber += (passCount - 1)
							break

			minSampleNumber += self.minSampleStep

		if minSampleNumber > upperBound:
			raise NameError("MinSampleNumberOverflow")

		return minSampleNumber

	def _calculateActualUpperBound(self, optClassifier, restrictionControl):		
		calculatedMinSample = self._minSampleCalculated(restrictionControl)

		print(calculatedMinSample)

		actualPercentage = 1.0 + (float(self.upperBoundPercetage)/ 100.0)

		return math.ceil(calculatedMinSample * actualPercentage)

