from abc import ABCMeta, abstractmethod
import math, random

class DistributionModel:
	__metaclass__ = ABCMeta

	@abstractmethod
	def pdf(self, x):
		pass

	@abstractmethod
	def sample(self):
		pass

	@abstractmethod
	def samples(self, count):
		pass

	@abstractmethod
	def probability(self, label):
		pass

	@abstractmethod
	def isDeterministic(self):
		pass

class NormalDistribution(DistributionModel):
	__metaclass__ = ABCMeta

	def __init__(self, probabilitiesClasses, mean, variation):
		self.mean = mean
		self.variation = variation
		self.probabilitiesClasses = probabilitiesClasses

	@abstractmethod
	def pdf(self, x):
		pass

	@abstractmethod
	def sample(self):
		pass

	@abstractmethod
	def samples(self, count):
		pass

	@abstractmethod
	def probability(self, label):
		pass

	def isDeterministic(self):
		return False

class ProbabilityBounds:
	def __init__(self, lowerBound, upperBound):
		self.lowerBound = lowerBound
		self.upperBound = upperBound

class NormalDistributionModel(NormalDistribution):
	def __init__(self, probabilitiesClasses, mean, variation):
		super(NormalDistributionModel, self).__init__(probabilitiesClasses, mean, variation)

	def pdf(self, x):
		exponent = -0.5*(((x - self.mean)/self.variation)**2.0)
		return (1.0/(math.sqrt(2.0*math.pi)*self.variation)) * math.exp(exponent)

	def sample(self):
		sample = random.random()	
		accumulativeProbabilityMap = self.__accumulativeProbabilityMap(self.probabilitiesClasses)
		
		for x in self.probabilitiesClasses:
			lowerBound = accumulativeProbabilityMap[x.label].lowerBound
			upperBound = accumulativeProbabilityMap[x.label].upperBound
			
			if lowerBound <= sample and sample < upperBound:
				return x.label
	
	def __accumulativeProbabilityMap(self, probabilitiesClasses):
		accumulativeProbabilityMap = {}
		accumulator = 0.0
		
		for x in probabilitiesClasses:
			px = self.probability(x.label)
			nextAccumulator = accumulator + px
			
			accumulativeProbabilityMap[x.label] = ProbabilityBounds(accumulator, nextAccumulator)
			accumulator = nextAccumulator
			
		return accumulativeProbabilityMap
			
	
	def samples(self, count):
		samples = []
		
		for i in range(0, count):
			samples.append(self.sample())
		
		return samples

	def probability(self, label):
		pdfs = {}
		pdfsSum = 0.0

		for x in self.probabilitiesClasses:
			xPdf = self.pdf(x.value)
			pdfsSum += xPdf
			
			pdfs[x.label] = xPdf

		return pdfs[label] / float(pdfsSum)


class UniformDistributionModel(DistributionModel):
	def __init__(self, probabilitiesObjects):
		self.probabilitiesClasses = probabilitiesObjects
		self.variation = "infinito"
	
	def pdf(self, x):
		return 1.0/float(len(self.probabilitiesClasses))

	
	def sample(self):
		drawnIndex = random.randint(0, len(self.probabilitiesClasses) - 1)
		return self.probabilitiesClasses[drawnIndex].label

	def samples(self, count):
		samples = []
		
		for i in range(0, count):
			samples.append(self.sample())
		
		return samples
	
	def probability(self, label):
		return 1.0/float(len(self.probabilitiesClasses))

	def isDeterministic(self):
		return False


