import json
from jsonschema import validate

class FromJsonFileToDictonaryParser:
	def __init__(self, schemaFilePath="JsonSchema/schema.json"):
		schemaFile = open(schemaFilePath)
		self.jsonSchema = json.load(schemaFile)


	def parse(self, jsonInputFilePath):
		jsonInputFile = open(jsonInputFilePath)
		jsonInput = json.load(jsonInputFile)
		validate(jsonInput, self.jsonSchema)

		return jsonInput