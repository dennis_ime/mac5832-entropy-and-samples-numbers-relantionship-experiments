from Parser.ProbabilityDistributionModelParsers import JsonDictornaryToDistributionModelArrayParser
from Parser.ClassifierGeneratorParser import JsonDictonaryToClassifierGeneratorParser
from Parser.RestrictControlParser import JsonDictonaryToRestrictControlParser
from Experiment.Experiment import Experiment

class JsonDictonaryToExperimentParser:

	def _initParsers(self, jsonDic):
		self.distributionModelParser = JsonDictornaryToDistributionModelArrayParser()
		parsedDistributionModels = self.distributionModelParser.parse(jsonDic["probabilitiesDistributions"])				
		self.classifierGeneratorParser = JsonDictonaryToClassifierGeneratorParser(parsedDistributionModels)
		self.restrictionControlsParser = JsonDictonaryToRestrictControlParser()

	def parse(self, jsonDic):
		self._initParsers(jsonDic)
		classifiersDic = jsonDic["classifierGenerators"]
		restrictionControls = []

		for classifierDic in classifiersDic:
			classifierGenerator = self.classifierGeneratorParser.parse(classifierDic)
			restrictionControl = self.restrictionControlsParser.parse(classifierGenerator, classifierDic)
			restrictionControls.append(restrictionControl)

		triesToPass = self._extractTriesToPass(jsonDic)
		upperBoundPercetage = self._extractUpperBoundPercetage(jsonDic)
		minSampleInitialValue = self._extractMinSampleInitialValue(jsonDic)
		minSampleStep = self._extractMinSampleStep(jsonDic)

		return Experiment(restrictionControls, triesToPass, upperBoundPercetage, minSampleInitialValue, minSampleStep)

	def _extractMinSampleStep(self, jsonDic):
		return jsonDic["minSampleStep"]

	def _extractMinSampleInitialValue(self, jsonDic):
		return jsonDic["minSampleInitialValue"]

	def _extractTriesToPass(self, jsonDic):
		return jsonDic["minConsecutivePassedToStop"]

	def _extractUpperBoundPercetage(self, jsonDic):
		return jsonDic["upperBoundInMinSamplesPercentage"]