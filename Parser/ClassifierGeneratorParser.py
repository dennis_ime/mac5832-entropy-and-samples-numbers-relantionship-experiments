from Parser.ProbabilityDistributionModelParsers import JsonDictornaryToDistributionModelArrayParser
from ProbabilityDistribution.ProbabilityClass import ProbabilityObject
from Classifier.Classifier import ClassifierGenerator, ClassifierVariable

class JsonDictonaryToClassifierGeneratorParser:
	def __init__(self, distributionModelParsed):
		self.distributionModel = distributionModelParsed

	def parse(self, jsonDic):
		randomVariableDistributionModel = self.distributionModel[jsonDic["distributionId"]]		
		classes = self.__extractClasses(jsonDic)
		variables = self.__extractVariables(jsonDic)

		return ClassifierGenerator(randomVariableDistributionModel, variables, classes)


	def __extractVariables(self, jsonDic):
		classifierVariables = []

		for variable in jsonDic["variables"]:
			variableDistributionModel = self.distributionModel[variable["classesDistributionId"]]
			classifierVariables.append(ClassifierVariable(variable["label"], variableDistributionModel))

		return classifierVariables

	def __extractClasses(self, jsonDic):
		classes = []

		for c in jsonDic["classes"]:
			classifierClass = ProbabilityObject(c["label"], c["value"])
			classes.append(classifierClass)

		return classes