from abc import ABCMeta, abstractmethod
from ProbabilityDistribution.DistributionModel import UniformDistributionModel, NormalDistributionModel
from ProbabilityDistribution.ProbabilityClass import ProbabilityObject

class JsonDictornaryToDistributionModelArrayParser:
	def parse(self, jsonDic):
		parsedDic = {}
		parser = None

		for e in jsonDic:
			if e["type"] == "gaussian":
				parser = JsonDictonaryToNormalDistributionParser()
			elif e["type"] == "uniform":
				parser = JsonDictonaryToUniformDistributionParser()

			parsedDistributionModel = parser.parse(e)
			parsedDic[e["id"]] = parsedDistributionModel

		return parsedDic

class JsonDictonaryToDistributionModelParser:
	__metaclass__ = ABCMeta

	@abstractmethod
	def parse(self, jsonDic):
		pass

	def _extractVariables(self, jsonDic):
		variables = []

		for variable in jsonDic["variables"]:
			variables.append(ProbabilityObject(variable["label"], variable["value"]))

		return variables


class JsonDictonaryToNormalDistributionParser(JsonDictonaryToDistributionModelParser):
	def parse(self, jsonDic):
		variation = self.__extractParameter(jsonDic, "variation")
		mean = self.__extractParameter(jsonDic, "mean")
		variables = self._extractVariables(jsonDic)

		return NormalDistributionModel(variables, mean, variation)

	def __extractParameter(self, jsonDic, paramName):
		firstParam = jsonDic["parameters"][0]
		secondParam = jsonDic["parameters"][1]

		if firstParam["name"] == paramName:
			return float(firstParam["value"])
		elif secondParam["name"] == paramName:
			return float(secondParam["value"])

		return 0.0

class JsonDictonaryToUniformDistributionParser(JsonDictonaryToDistributionModelParser):
	def parse(self, jsonDic):
		variables = self._extractVariables(jsonDic)
		return UniformDistributionModel(variables)

