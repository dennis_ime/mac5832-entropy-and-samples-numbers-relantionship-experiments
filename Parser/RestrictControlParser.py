from Parser.ProbabilityDistributionModelParsers import JsonDictornaryToDistributionModelArrayParser
from Parser.ClassifierGeneratorParser import JsonDictonaryToClassifierGeneratorParser
from RestrictionControl.ErrorMeasurer  import *

from RestrictionControl.RestrictionControl import RestrictionControl

class JsonDictonaryToRestrictControlParser:
	def parse(self, classifierGenerator, jsonDic):
		epsilon = jsonDic["epsilon"]
		delta = jsonDic["delta"]
		numberOfTries = self.__extractNumberOfTries(jsonDic)
		errorMeasurer = self.__extractErrorMeasurer(jsonDic)
		spaceCardinality = self.__extractSpaceCardinality(jsonDic)
		return self.__createRestrictControl(errorMeasurer, classifierGenerator, spaceCardinality, delta, epsilon, numberOfTries)

	def __createRestrictControl(self, errorMeasurer, classifierGenerator, spaceCardinality,  delta, epsilon, numberOfTries):		
		if numberOfTries is None:
			return RestrictionControl(errorMeasurer, classifierGenerator, spaceCardinality, delta, epsilon)
		else:
			return RestrictionControl(errorMeasurer, classifierGenerator, spaceCardinality, delta, epsilon, numberOfTries)

	def __extractNumberOfTries(self, jsonDic):
		if "numberOfTries" in jsonDic:
			return None
		else:
			return jsonDic["numberOfTries"]

	def __extractErrorMeasurer(self, jsonDic):
		if not "errorMeasure" in jsonDic:
			return ConditionalProbabilityErrorMeasurer()


		errorMeasureType = jsonDic["errorMeasure"]

		lowerMeasureType = errorMeasureType.lower()
		if lowerMeasureType == "conditionalprobability":
			return ConditionalProbabilityErrorMeasurer()
		elif lowerMeasureType == "sameclasserror":
			return SameClassErrorMeasurer()
		elif lowerMeasureType == "conditionalprobabilitywithoptimumclassifier":
			return ConditionalProbabilityWithOptClassifierClassErrorMeasurer()
		elif lowerMeasureType == "optimalmae":
			return OptimalMAEErrorMeasurer()
		elif lowerMeasureType == "conditionalprobabilitywithoptclassifierclassusingdistributionmodelerrormeasurer":
			return ConditionalProbabilityWithOptClassifierClassUsingDistributionModelErrorMeasurer()
		elif lowerMeasureType == "riskerrormeasurer":
			return RiskErrorMeasurer()
		else:
			return ConditionalProbabilityErrorMeasurer()

	def __extractSpaceCardinality(self, jsonDic):
		return jsonDic["spaceCardinality"]