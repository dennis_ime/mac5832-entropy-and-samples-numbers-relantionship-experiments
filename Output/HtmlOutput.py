from Output.Ploting import CartesianSystemPlotting
from shutil import copy2
import os


class HtmlOutput:
	def __init__(self, pathToTemplate):
		templateFile = open(pathToTemplate)
		self.templateText = templateFile.read()		
		templateFile.close()

		self.reset()

	def injectContent(self, placeHolderId, content):
		self.htmlText = self.htmlText.replace("[% " + placeHolderId + " %]", content)

	def getHtml(self):
		return  self.htmlText

	def reset(self):
		self.htmlText = self.templateText

	def saveOnFile(self, pathToSave):
		outputFile = open(pathToSave, "w")
		outputFile.write(self.getHtml())
		outputFile.close()


class ExperimentOutput:

	IMAGE_FILENAME = "plotting.png"
	HTML_PAGE_NAME = "index.html"

	MIN_SAMPLE_CALCULATED_ID = "minSamplesCalculeted"
	ENTROPY_ID = "entropy"
	MIN_SAMPLE_ID = "minSample"
	CLASSIFIER_TABLE_ID = "classifierTable"
	IMAGE_SRC = "imagePlaceHolder"
	PLOTTING_POINTS = "plottingPoints"
	LINK_JSON_CONFIG_ID = "linkJsonConfig"
	VARIATION_ID = "variation"

	def __init__(self, pageTemplatePath="HTMLTemplate/PageTemplate.html", classifierWidgetTemplatePath="HTMLTemplate/PlottingPointsTemplate.html"):
		self.pageTemplate = HtmlOutput(pageTemplatePath)
		self.classifierWidgetTemplate = HtmlOutput(classifierWidgetTemplatePath)
		self.plotter = CartesianSystemPlotting()
		self.plotter.xLabel = "entropia"
		self.plotter.yLabel = "número minimo de amostras"

	def createHtmlOutput(self, plottingPoints, minSampleCalculated, delta, epsilon, configFilePath, outputDirectory):		
		classifiersTableList = []
		self.plotter.title = "Entropia X Número minímo de amostras para delta = " + str(delta) + ", epsilon = " + str(epsilon)

		for p in plottingPoints:			
			classifierHtmlTable = p.optimumClassifier.toHtmlTable()
			entropy = p.point.entropy
			minSamples = p.point.minSamples
			variation = p.variation

			self.classifierWidgetTemplate.injectContent(ExperimentOutput.ENTROPY_ID, str(entropy))
			self.classifierWidgetTemplate.injectContent(ExperimentOutput.MIN_SAMPLE_ID, str(minSamples))
			self.classifierWidgetTemplate.injectContent(ExperimentOutput.CLASSIFIER_TABLE_ID, classifierHtmlTable)
			self.classifierWidgetTemplate.injectContent(ExperimentOutput.VARIATION_ID, str(variation))

			classifiersTableList.append(self.classifierWidgetTemplate.getHtml())
			self.classifierWidgetTemplate.reset()
			self.plotter.addXYPair(entropy, minSamples)


		self._createDirectoryIfItDoesNotExist(outputDirectory)
		
		imageFilePath = outputDirectory + "/" + ExperimentOutput.IMAGE_FILENAME		
		self.plotter.plotOnImageFile(imageFilePath)

		classifiersTableFinalHtml = "\n".join(classifiersTableList)
		
		copy2(configFilePath, outputDirectory)

		self.pageTemplate.injectContent(ExperimentOutput.MIN_SAMPLE_CALCULATED_ID, str(minSampleCalculated))
		self.pageTemplate.injectContent(ExperimentOutput.IMAGE_SRC, ExperimentOutput.IMAGE_FILENAME)
		self.pageTemplate.injectContent(ExperimentOutput.PLOTTING_POINTS, classifiersTableFinalHtml)
		self.pageTemplate.injectContent(ExperimentOutput.LINK_JSON_CONFIG_ID, os.path.basename(configFilePath))

		self.pageTemplate.saveOnFile(outputDirectory + "/" + ExperimentOutput.HTML_PAGE_NAME)

	def _createDirectoryIfItDoesNotExist(self, path):
		if not os.path.exists(path):
			os.makedirs(path)


