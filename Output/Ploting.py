import matplotlib.pyplot as plt

class Point:
	def __init__(self, entropy, minSamples):
		self.entropy = entropy
		self.minSamples = minSamples

class PlottingPoint:
	def __init__(self, point, optimumClassifier, variation):
		self.point = point
		self.optimumClassifier = optimumClassifier
		self.variation = variation


class CartesianSystemPlotting(object):	
	def __init__(self):
		self.xLabel = "undefined"
		self.yLabel = "undefined"
		self.title = "undefined"
		self.xValues = []
		self.yValues = []

	def addXYPair(self, x, y):
		self.xValues.append(x)
		self.yValues.append(y)

	def plotOnScreen(self):
		self._configPlot()
		plt.show()

	def plotOnImageFile(self, filepath):
		self._configPlot()
		plt.savefig(filepath)

	def _configPlot(self):
		plt.xlabel(self.xLabel)
		plt.ylabel(self.yLabel)
		plt.title(self.title)
		plt.grid(True)
		plt.plot(self.xValues, self.yValues)
		