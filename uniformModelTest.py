from ProbabilityDistribution.DistributionModel import NormalDistributionModel, UniformDistributionModel
from ProbabilityDistribution.ProbabilityClass import ProbabilityObject
from Classifier.Classifier import Classifier, ClassifierGenerator, ClassifierVariable
from Classifier.Accumulator import AccumulatorLabel, AccumulatorProbabilityCalculator
from RestrictionControl.RestrictionControl import ConditionalProbabilityErrorMeasurer, RestrictionControl


probabilitiesClasses = [ProbabilityObject("1", 1.0), ProbabilityObject("2", 2.0), ProbabilityObject("3", 3.0),
						ProbabilityObject("4", 4.0),ProbabilityObject("5", 5.0)]

#dist = UniformDistributionModel(probabilitiesClasses)
dist = NormalDistributionModel(probabilitiesClasses, 3.0,  0.311111111111)

xs = [ProbabilityObject("0000", 0), ProbabilityObject("0001", 1), ProbabilityObject("0010", 2), ProbabilityObject("0011", 3),
	  ProbabilityObject("0100", 4), ProbabilityObject("0101", 5), ProbabilityObject("0110", 6), ProbabilityObject("0111", 7), 
	  ProbabilityObject("1000", 8), ProbabilityObject("1001", 9), ProbabilityObject("1010", 10), ProbabilityObject("1011", 11),
	  ProbabilityObject("1100", 12), ProbabilityObject("1101", 13), ProbabilityObject("1110", 14), ProbabilityObject("1111", 15)]


cs = [ClassifierVariable(xs[0].label, dist), ClassifierVariable(xs[1].label, dist), ClassifierVariable(xs[2].label, dist),
	  ClassifierVariable(xs[3].label, dist), ClassifierVariable(xs[4].label, dist), ClassifierVariable(xs[5].label, dist),
	  ClassifierVariable(xs[6].label, dist), ClassifierVariable(xs[7].label, dist), ClassifierVariable(xs[8].label, dist),
	  ClassifierVariable(xs[9].label, dist), ClassifierVariable(xs[10].label, dist), ClassifierVariable(xs[11].label, dist),
	  ClassifierVariable(xs[12].label, dist), ClassifierVariable(xs[13].label, dist), ClassifierVariable(xs[14].label, dist),
	  ClassifierVariable(xs[15].label, dist)]

uni = UniformDistributionModel(xs)


classifierGenerator = ClassifierGenerator(uni, cs, probabilitiesClasses)
optClassifier = classifierGenerator.generateOptimumClassifier()
classifier = classifierGenerator.generateClassifier(1)
errorMeasure = ConditionalProbabilityErrorMeasurer()
restrictionControl = RestrictionControl(errorMeasure, classifierGenerator, 152587890625)

print(restrictionControl.hasPassed(1))
